from django.contrib import admin
from .models import Information, Accordion
# Register your models here.

admin.site.register(Information)
admin.site.register(Accordion)
