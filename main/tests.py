from django.test import LiveServerTestCase, TestCase, tag
from django.urls import reverse, resolve
from .models import Accordion, Information
from .views import home
# from selenium import webdriver


class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)

    def test_model(self):
        accordion_model = Accordion(
            title='Organization'
        )
        accordion_model.save()
        information = Information(
            info_name='RISTEK',
            info_content='RISTEK merupakan organisasi di Fasilkom UI yang mewadahi \
                mahasiswa yang memiliki ketertarikan pada teknologi',
            accordion= accordion_model
        )
        information.save()
        self.assertEqual(accordion_model.title, 'Organization')
        self.assertEqual(information.info_name, 'RISTEK')

        self.assertEqual(information.accordion, accordion_model)
        self.assertEqual(accordion_model.information_set.get(info_name='RISTEK'), information)

    def test_view(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_template_used(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'main/home.html')
# @tag('functional')
# class FunctionalTestCase(LiveServerTestCase):
#     """Base class for functional test cases with selenium."""

#     @classmethod
#     def setUpClass(cls):
#         super().setUpClass()
#         # Change to another webdriver if desired (and update CI accordingly).
#         options = webdriver.chrome.options.Options()
#         # These options are needed for CI with Chromium.
#         options.headless = True  # Disable GUI.
#         options.add_argument('--no-sandbox')
#         options.add_argument('--disable-dev-shm-usage')
#         cls.selenium = webdriver.Chrome(options=options)

#     @classmethod
#     def tearDownClass(cls):
#         cls.selenium.quit()
#         super().tearDownClass()




# class MainFunctionalTestCase(FunctionalTestCase):
#     def test_root_url_exists(self):
#         self.selenium.get(f'{self.live_server_url}/')
#         html = self.selenium.find_element_by_tag_name('html')
#         self.assertNotIn('not found', html.text.lower())
#         self.assertNotIn('error', html.text.lower())
