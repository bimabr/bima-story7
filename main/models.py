from django.db import models

# Create your models here.
class Accordion(models.Model):
    title = models.CharField(max_length=255)

class Information(models.Model):
    info_name = models.CharField(max_length=255)
    info_content = models.TextField()
    accordion = models.ForeignKey(Accordion, models.RESTRICT)