
$('.pack > li > .content').hide();

$('.pack > li').on('click', function () {
    var id = this.getAttribute('data-id');
    if ($(this).hasClass('active')) {
        $(this).removeClass('active').find('.content').slideUp(function () {
            $(`button[data-id="${id}"]`).fadeIn();
        });
    } else {
        $(`button[data-id="${id}"]`).fadeOut();

        // hilangkan class active pada child list yg lain
        // statement ini diperlukan saat sedang ada accordion yg terbuka
        // lalu langsung membuka accordion yg lainnya
        $('.pack > li.active .content').slideUp();
        var p = $(`.pack > li.active`).attr('data-id');
        $(`button[data-id=${p}]`).fadeIn();
        $('.pack > li.active ').removeClass('active');

        $(this).addClass('active').find('.content').slideDown();
    }
    return false;
});

function moveChoiceTo(elem_choice, direction) {
    var id = elem_choice.getAttribute('data-id');
    var parentElem = document.getElementById(`${id}`);
    var pack = parentElem.parentNode;
    var ul = parentElem.parentNode.parentNode;

    if (direction === -1 && pack.previousElementSibling) {
        // Naikkan div jika terdapat div lagi di atasnya
        ul.insertBefore(pack, pack.previousElementSibling);
    } else if (direction === 1 && parentElem.nextElementSibling) {
        // Turunkan div jika terdapat div lagi di bawahnya
        ul.insertBefore(pack, pack.nextElementSibling.nextElementSibling);
    }
    
}