from django.shortcuts import render
from .models import Accordion, Information

def home(request):
    data = {
        'list_of_accordion': Accordion.objects.all()
    }
    return render(request, 'main/home.html', data)
